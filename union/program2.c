#include<stdio.h>
union employee{
	int empid;
	float sal;
};

void main(){
	
	//problem ===>  union employee emp1={10,50.60};
	union employee emp2;
	emp2.empid=15;
	printf("%d\n",emp2.empid);
	
	emp2.sal=70.65;
	printf("%f\n",emp2.sal);

}
