#include<stdio.h>
void main(){
	int x=10;
	int *ptr1=&x;
	int *ptr2=x;     //warning: initialization of ‘int *’ from ‘int’ makes pointer from integer without a cast

	printf("%p\n",ptr1);
	printf("%p\n",ptr2);    //hexadecimal value of x
}
