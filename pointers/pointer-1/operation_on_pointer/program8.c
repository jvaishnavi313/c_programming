#include<stdio.h>
void main(){
	char ch1='x';
	char ch2='y';
	char *ptr=&ch1;

	printf("%p\n",ptr);
	printf("%c\n",*ptr);

	printf("%p\n",(ptr+1.5));     //error: invalid operands to binary + (have ‘char *’ and ‘double’)
	printf("%c\n",*(ptr+1.5));    //error: invalid operands to binary + (have ‘char *’ and ‘double’)

}

