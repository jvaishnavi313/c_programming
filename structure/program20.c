#include<stdio.h>
struct demo{
	int x;
	float y;
};
void main(){
	struct demo obj={10,20.5f};
	int arr[]={10,20,30,40,50};
	printf("%p\n",&arr[0]);          //200
	printf("%p\n",arr);             //200
	printf("%p\n",&obj.x);        //100
	printf("%p\n",obj);      //error
	
	//printf("%p\n",&obj); ===>100

}

