//initialisation by initializer list

#include<stdio.h>
void main(){
	int iarr[5]={10,20,30,40,50};
	printf("%d\n",iarr);      // warning: format ‘%d’ expects argument of type ‘int’, but argument 2 has type ‘int *’
				  // name of array means address of first element
	printf("%p\n",iarr);
}
