#include<stdio.h>
void main(){

	int arr[2][3];
	printf("enter array elements\n");

	for(int row=0;row<2;row++){
		for(int col=0;col<3;col++){
			scanf("%d",&arr[row][col]);
		}
	}

	printf("entered array elements\n");
	for(int row=0;row<2;row++){
		for(int col=0;col<3;col++){
			printf("%d  ",arr[row][col]);
		}
		printf("\n");
	}

	for(int i=0;i<6;i++){
		printf("%d\n",arr[i]);   //warning: format ‘%d’ expects argument of type ‘int’, but argument 2 has type ‘int *’
					 //arr[i] in 2D array is address of first element of [i] row.
	}
}
	
