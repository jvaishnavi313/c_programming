//sum of diagonal
#include<stdio.h>
void main(){
	int arr[3][3]={10,20,30,40,50,60,70,80,90};
	int sum=0;

	for(int row=0;row<3;row++){
		for(int col=0;col<3;col++){
			printf("%d   ",arr[row][col]);
			if(row==col){
				sum=sum+arr[row][col];
			}
		}
		printf("\n");
	}
	printf("sum of diagonal elements are %d\n",sum);
}


