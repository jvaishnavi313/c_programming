#include<stdio.h>
int diagonalsum1(int (*ptr)[3],int row,int col){
	int sum=0;
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			if(i==j){
				sum=sum+*(*(ptr+i)+j);
			}
		}
	}
	return sum;
}
int diagonalsum2(int (*ptr)[3],int arrsize){
	int sum=0;
	for(int i=0;i<arrsize;i+=4){
			sum=sum+*(*ptr+i);
	}
	return sum;
}
void main(){

	int arr[3][3]={1,2,3,4,5,6,7,8,9};
	int arrsize=sizeof(arr)/sizeof(int);
	int row=3,col=3;

	int sum1=diagonalsum1(arr,row,col);
	int sum2=diagonalsum2(arr,arrsize);

	printf("%d\n",sum1);
	printf("%d\n",sum2);

}


