#include<stdio.h>
void add(int a,int b){
	printf("%d\n",a+b);
}
void sub(int a,int b){
	printf("%d\n",a-b);
}
void main(){
	void (*ptr)(int);
	ptr=add;       //warning: assignment to ‘void (*)(int)’ from incompatible pointer type ‘void (*)(int,  int)’
	ptr(10,20);     //error: too many arguments to function ‘ptr’
	ptr=sub;
	ptr(30,10);

}
