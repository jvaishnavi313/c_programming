#include<stdio.h>
void add(int a,int b){
	printf("%d\n",a+b);
}
void sub(int a,int b){
	printf("%d\n",a-b);
}
void main(){
	int (*ptr)(int,int);
	ptr=add;     //warning: assignment to ‘int (*)(int,  int)’ from incompatible pointer type ‘void (*)(int,  int)’ 
	ptr(10,20);
	ptr=sub;      //warning: assignment to ‘int (*)(int,  int)’ from incompatible pointer type ‘void (*)(int,  int)’ 
	ptr(30,10);

}
