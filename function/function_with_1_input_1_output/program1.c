#include<stdio.h>
void main(){

	fun(10);    //warning: implicit declaration of function ‘fun’
	fun('A');
}
void fun(int x){     //warning: conflicting types for ‘fun’; have ‘void(int)’
	printf("%d\n",x);
}

