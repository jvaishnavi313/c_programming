#include<stdio.h>
void fun(int *x){
	printf("%p\n",x);
}
void main(){
	int a=10;
	fun(a);   // warning: passing argument 1 of ‘fun’ makes pointer from integer without a cast 
		 
}
